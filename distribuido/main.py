import json
import socket
import sys
import os
from time import sleep
import _thread
import threading

import RPi.GPIO as GPIO
import adafruit_dht as DHT
import board

from components import In, Out, Temperature
from sensors import Sensors
from outputs import Outputs
from connection import Connection

jsonfile = open('configuracao_sala_01.json')
jsondata = json.load(jsonfile)

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

hostCentralServer: str = jsondata['ip_servidor_central']
portCentralServer: str = jsondata['porta_servidor_central']
hostDistributed = ''
portDistributed = jsondata['porta_servidor_distribuido']

lamp1 = Out('L_01', jsondata['outputs'][0]['gpio'])
lamp2 = Out('L_02', jsondata['outputs'][1]['gpio'])
airConditioner = Out('AC', jsondata['outputs'][3]['gpio'])
multiMediaP = Out('PR', jsondata['outputs'][2]['gpio'])
alarm = Out('AL_BZ', jsondata['outputs'][4]['gpio'])
outList = [lamp1.channel, lamp2.channel, airConditioner.channel, multiMediaP.channel, alarm.channel]

presence = In('SPres', jsondata['inputs'][0]['gpio'])
smoke = In('SFum', jsondata['inputs'][1]['gpio'])
window = In('SJan', jsondata['inputs'][2]['gpio'])
door = In('SPor', jsondata['inputs'][3]['gpio'])
walkIn = In('SC_IN', jsondata['inputs'][4]['gpio'])
walkOut = In('SC_OUT', jsondata['inputs'][5]['gpio'])
inList = [presence.channel, smoke.channel, window.channel, door.channel, walkIn.channel, walkOut.channel]

temperature = Temperature('DHT22', jsondata['sensor_temperatura'][0]['gpio'])
dhtDevice = DHT.DHT22(board.D4)

def startInOut():
    GPIO.setup(outList, GPIO.OUT)
    GPIO.setup(inList, GPIO.IN)

def main():
    sensors: Sensors = Sensors(presence, temperature, smoke, door, window, walkIn, walkOut, dhtDevice, alarm)
    outputs: Outputs = Outputs(lamp1, lamp2, airConditioner, multiMediaP, dhtDevice)
    connection: Connection = Connection(jsondata['nome'], hostCentralServer, portCentralServer, hostDistributed, portDistributed, outputs, sensors)

    try:
        t1 = threading.Thread(target = sensors.watchSensors, args = ())
        t1.start()

        t2 = threading.Thread(target = sensors.countPeople, args = ())
        t2.start()

        t3 = threading.Thread(target = outputs.watchOutputs, args = ())
        t3.start()

        t4 = threading.Thread(target = connection.sendAllInfo, args = ())
        t4.start()

        t5 = threading.Thread(target = connection.receiveFromServer, args = ())
        t5.start()

    except:
        print ("Error: unable to start threads")
        return

    t1.join()
    t2.join()
    t3.join()
    t4.join()
    t5.join()

startInOut()
main()
jsonfile.close()