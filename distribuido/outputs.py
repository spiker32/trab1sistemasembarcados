from components import Out
from time import sleep
import os
import json

import RPi.GPIO as GPIO
import adafruit_dht as DHT
import board

class Outputs:
    def __init__(self, lamp1: Out, lamp2: Out, airConditioner: Out, multiMediaP: Out, dhtDevice: any) -> None:
        self.lamp1 = lamp1
        self.lamp2 = lamp2
        self.airConditioner = airConditioner
        self.multiMediaP = multiMediaP
        self.dhtDevice = dhtDevice

    def readOut(self, output: Out) -> None:
        state = GPIO.input(output.channel)
        if state == 1:
            output.state = True
        else:
            output.state = False

    def setOutput(self, output: Out, state: bool) -> None:
        if state == True:
            GPIO.output(output.channel, GPIO.HIGH)
            output.state = True
        else:
            GPIO.output(output.channel, GPIO.LOW)
            output.state = False

    def watchOutputs(self) -> None:
        while True:
            self.readOut(self.lamp1)
            self.readOut(self.lamp2)
            self.readOut(self.airConditioner)
            self.readOut(self.multiMediaP)
            sleep(2)

    def setJsonStates(self, outputs: list[object]) -> None:
        
        for output in outputs:
            if(output['name'] == 'lamp1'):
                self.setOutput(self.lamp1, output['state'])
            elif(output['name'] == 'lamp2'):
                self.setOutput(self.lamp2, output['state'])
            elif(output['name'] == 'airConditioner'):
                self.setOutput(self.airConditioner, output['state'])
            elif(output['name'] == 'multiMediaP'):
                self.setOutput(self.multiMediaP, output['state'])