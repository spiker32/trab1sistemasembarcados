from components import In, Temperature, Out
from time import sleep
import os
import json
import threading

import RPi.GPIO as GPIO
import adafruit_dht as DHT
import board

class Sensors:
    def __init__(self, presence: In, temperature: Temperature, smoke: In, door: In, window: In, walkIn: In, walkOut: In, dhtDevice: any, alarm: Out) -> None:
        self.presence = presence
        self.temperature = temperature
        self.smoke = smoke
        self.door = door
        self.window = window
        self.walkIn = walkIn
        self.walkOut = walkOut
        self.dhtDevice = dhtDevice
        self.alarm = alarm
        self.totalPeople: int = 0
        self.alarmSystem: bool = False
        self.smokeSystem: bool = False
        self.needToSend: bool = False

    def readTemperatureAndUmidity(self) -> None:
        while True:
            try:
                self.temperature.temperature = self.dhtDevice.temperature
                self.temperature.humidity = self.dhtDevice.humidity
                break
            except RuntimeError:
                continue
            except Exception:
                continue

    def readIn(self, input: In):
        state = GPIO.input(input.channel)
        if state == 1:
            input.state = True
        else:
            input.state = False

    def activateAlarmBuzzer(self):
        GPIO.output(self.alarm.channel, GPIO.HIGH)

    def watchSensors(self):
        counter: int = 0
        counterTemperatura: int = 20

        pastPresence = self.presence.state
        pastSmoke = self.smoke.state
        pastDoor = self.door.state
        pastWindow = self.window.state

        while True:
            self.readIn(self.presence)
            self.readIn(self.smoke)
            self.readIn(self.door)
            self.readIn(self.window)
            self.readIn(self.alarm)

            if(self.alarmSystem and (self.alarm.state == False)):
                if(self.door.state or self.window.state or self.presence.state):
                    self.activateAlarmBuzzer()

            if(self.smokeSystem == True and self.smoke.state == True and self.alarm.state == False):
                self.activateAlarmBuzzer()

            if(counter == counterTemperatura):
                self.readTemperatureAndUmidity()
                self.sendToCentral()
                counter = 0

            if(pastPresence != self.presence.state or pastSmoke != self.smoke.state or pastDoor != self.door.state or pastWindow != self.window.state):
                self.sendToCentral()

            pastPresence = self.presence.state
            pastSmoke = self.smoke.state
            pastDoor = self.door.state
            pastWindow = self.window.state         

            counter = counter + 1
            sleep(0.1)

    def countPeople(self):
        pastIn = False
        pastOut = False
        while True:
            self.readIn(self.walkIn)
            self.readIn(self.walkOut)

            if self.walkIn.state == True and pastIn == False:
                pastIn = True
                self.totalPeople = self.totalPeople + 1
            if self.walkIn.state == False and pastIn == True:
                pastIn = False
            
            if self.walkOut.state == True and pastOut == False:
                pastOut = True
                self.totalPeople = self.totalPeople - 1
            if self.walkOut.state == False and pastOut == True:
                pastOut = False
            
            sleep(0.05)

    def sendToCentral(self):
        self.needToSend = True