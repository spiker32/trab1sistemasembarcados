import socket
import json
from outputs import Outputs
from sensors import Sensors
from components import In, Out, Temperature
from time import sleep

class Connection:
    def __init__(self, name:str, hostCentralServer: str, portCentralServer: str, hostDistributed: str, portDistributed: str, outputs: Outputs, sensors: Sensors) -> None:
        self.name = name
        self.hostCentralServer = hostCentralServer
        self.portCentralServer = portCentralServer
        self.hostDistributed = hostDistributed
        self.portDistributed = portDistributed
        self.outputs = outputs
        self.sensors = sensors


    def convertStringToJsonByte(self, message: str):
        byteData = json.dumps(message)
        byteData = bytes(byteData, encoding="UTF-8")
        return byteData


    def sendToServer(self, message: dict[str, any]):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            sock.connect((self.hostCentralServer, self.portCentralServer))
        except:
            return

        try: 
            sock.sendall(self.convertStringToJsonByte(message))

        finally:
            sock.close()

    def receiveFromServer(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            sock.bind((self.hostDistributed, self.portDistributed))
        except socket.error as msg:
            print("Erro ao criar socket: ", msg)
            exit(1)
            
        sock.listen(1)

        while True:
            connection, client_address = sock.accept()
            try:
                while True:
                    data = connection.recv(2048)
                        
                    if data:
                        decodedData = json.loads(data.decode('UTF-8'))
                        try:
                            outputs: list[object] = decodedData['outputs']
                            self.outputs.setJsonStates(outputs)
                        except:
                            try:
                                alarmState: bool = decodedData['alarm']
                                self.sensors.alarmSystem = alarmState
                            except:
                                try:
                                    smokeAlarmState: bool = decodedData['fireAlarm']
                                    self.sensors.smokeSystem = smokeAlarmState
                                except:
                                    pass
                    else:
                        break
                    
            finally:
                connection.close()
        

    def sendAllInfo(self):
        while True:
            if(self.sensors.needToSend == True):
                self.sensors.needToSend = False
                data = {
                    "name": self.name,
                    "alarm": self.sensors.alarmSystem,
                    "fireAlarm": self.sensors.smokeSystem,
                    "outputs": [
                            {
                                "name": "lamp1",
                                "state": self.outputs.lamp1.state
                            },
                            {
                                "name": "lamp2",
                                "state": self.outputs.lamp2.state
                            },
                            {
                                "name": "airConditioner",
                                "state": self.outputs.airConditioner.state
                            },
                            {
                                "name": "multiMediaP",
                                "state": self.outputs.multiMediaP.state
                            }
                    ],
                    "sensors": [
                        {
                            "name": "presence",
                            "state": self.sensors.presence.state
                        },
                        {
                            "name": "smoke",
                            "state": self.sensors.smoke.state
                        },
                        {
                            "name": "window",
                            "state": self.sensors.window.state
                        },
                        {
                            "name": "door",
                            "state": self.sensors.door.state
                        },
                        {
                            "name": "walkIn",
                            "state": self.sensors.walkIn.state
                        }
                        ,
                        {
                            "name": "walkOut",
                            "state": self.sensors.walkOut.state
                        },
                        {
                            "name": "alarm",
                            "state": self.sensors.alarm.state
                        }
                    ],
                    "temperature":[
                        {
                            "name": "temperature",
                            "value": self.sensors.temperature.temperature
                        },
                        {
                            "name": "humidity",
                            "value": self.sensors.temperature.humidity
                        }
                    ],
                    "totalPeople": self.sensors.totalPeople
                }
                self.sendToServer(data)
            sleep(0.05)
