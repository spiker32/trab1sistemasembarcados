class In:
    def __init__(self, sigla, channel):
        self.sigla = sigla
        self.channel = channel
        self.state = False

class Out:
    def __init__(self, sigla, channel):
        self.sigla = sigla
        self.channel = channel
        self.state = False

class Temperature:
    def __init__(self, sigla, channel):
        self.sigla = sigla
        self.channel = channel
        self.humidity = 0.0
        self.temperature = 0.0