# Trab1SistemasEmbarcados

Necessário instalar:
- Python3
- Dependências: RPi.GPIO e adafruit

Para rodar o servidor central:
- Configurar o IP e portas corretamento no arquivo configuracao_sala_01.json
- Rodar o comando: python main.py

Para rodar os servidores distribuidos:
- Configurar o IP e portas corretamento no arquivo configuracao_sala_01.json ou configuracao_sala_02.json
- Dentro da main.py, na linha 18 escrever 'configuracao_sala_01.json' ou 'configuracao_sala_02.json' a depender da configuração
- Rodar o comando: python main.py
