from connection import Connection
from distributed import Distributed
from time import sleep
import os

class Terminal:
    def __init__(self, connections: Connection) -> None:
        self.connections = connections


    def menu(self):
        while True:
            print("-----------------------------------------------")
            print("1 -> Listar informações de todas as salas")
            print("2 -> Menu específico de sala")
            print("3 -> Alarmes")
            print("4 -> Ligar todas as lâmpadas do prédio")
            print("5 -> Desligar todas as cargas do prédio")

            value = input()
            self.activateOption(value)


    def activateOption(self, value: str):
        os.system('clear')

        if(value == '1'):
            self.showAllInformation()
        elif(value == '2'):
            self.chooseRoom()
        elif(value == '3'):
            self.alarmSystem()
        elif(value == '4'):
            self.turnOnAllLamps()
        elif(value == '5'):
            self.turnOffAllBuilding()
        else:
            print("Opção inválida!")


    def turnOnAllLamps(self):
        message = {"outputs": [{"name": "lamp1", "state": True}, {"name": "lamp2", "state": True}]}
        for distributed in self.connections.distributedList:
            self.sendInformation(distributed, message)


    def turnOffAllBuilding(self):
        message = {"outputs": [{"name": "lamp1", "state": False}, {"name": "lamp2", "state": False}, {"name": "airConditioner", "state": False}, {"name": "multiMediaP", "state": False}]}
        for distributed in self.connections.distributedList:
            self.sendInformation(distributed, message)


    def alarmSystem(self):
        while True:
            os.system('clear')
            print("1  -> Ativar sistema de alarme")
            print("11 -> Desligar sistema de alarme")
            print("2  -> Ativar sistema de alarme de incêndio")
            print("22 -> Desligar sistema de alarme de incêndio")
            print("0  -> Voltar ao menu")

            value = input()

            if(value == '0'):
                return
            elif(value == '1'):
                message = {"alarm": True}
            elif(value == '11'):
                message = {"alarm": False}
            elif(value == '2'):
                message = {"fireAlarm": True}
            elif(value == '22'):
                message = {"fireAlarm": False}
            else:
                print("Entrada inválida!")
                continue

            for distributed in self.connections.distributedList:
                self.sendInformation(distributed, message)
        

    def chooseRoom(self):
        i: int = 0
        os.system('clear')
        print("Escolha a sala para acessar os comandos:")
        for distributed in self.connections.distributedList:
            print(i, " -> ","Para escolher ", distributed.information['name'])
            i = i + 1

        value = input()

        try:
            numberValue = int(value)
        except:
            print("Opção inválida!")
            return

        if(numberValue < 0 or numberValue > (len(self.connections.distributedList) - 1)):
            print("Opção inválida!")
            return

        self.roomOptions(self.connections.distributedList[numberValue])


    def roomOptions(self, room: Distributed):
        while True:
            os.system('clear')
            print("1  -> Ligar lâmpada 1")
            print("11 -> Desligar lâmpada 1")
            print("2  -> Ligar lâmpada 2")
            print("22 -> Desligar lâmpada 2")
            print("3  -> Ligar ar-condicionado")
            print("33 -> Desligar ar-condicionado")
            print("4  -> Ligar projetor")
            print("44 -> Desligar projetor")
            print("0  -> Retornar para menu principal")
            value = input()

            if(value == '0'):
                return
            elif(value == '1'):
                message = {"outputs": [{"name": "lamp1", "state": True}]}
            elif(value == '11'):
                message = {"outputs": [{"name": "lamp1", "state": False}]}
            elif(value == '2'):
                message = {"outputs": [{"name": "lamp2", "state": True}]}
            elif(value == '22'):
                message = {"outputs": [{"name": "lamp2", "state": False}]}
            elif(value == '3'):
                message = {"outputs": [{"name": "airConditioner", "state": True}]}
            elif(value == '33'):
                message = {"outputs": [{"name": "airConditioner", "state": False}]}
            elif(value == '4'):
                message = {"outputs": [{"name": "multiMediaP", "state": True}]}
            elif(value == '44'):
                message = {"outputs": [{"name": "multiMediaP", "state": False}]}
            else:
                print("Entrada inválida!")
                continue
                
            self.sendInformation(room, message)


    def sendInformation(self, target: Distributed, message: dict[str, any]):
            self.connections.sendInformation(target, message)


    def showAllInformation(self):
        if(len(self.connections.distributedList) < 1):
            print("Nenhuma conexão ativa")
            return

        allPeopleInBuilding: int = 0
        fireAlarmSystem: bool = False
        alarmSystem: bool = False
        

        for distributed in self.connections.distributedList:
            all = distributed.information

            print("-----------------------------------------------")
            print("Nome: ", all['name'])

            print("Lâmpada 1: ", all['outputs'][0]['state'])
            print("Lâmpada 2: ", all['outputs'][1]['state'])
            print("Ar Condicionado: ", all['outputs'][2]['state'])
            print("Projetor Multimídia: ", all['outputs'][3]['state'])

            print("Sensor de presença: ", all['sensors'][0]['state'])
            print("Sensor de fumaça: ", all['sensors'][1]['state'])
            print("Sensor de janela: ", all['sensors'][2]['state'])
            print("Sensor de porta: ", all['sensors'][3]['state'])
            print("Alarme (buzzer): ", all['sensors'][6]['state'])

            print("Temperatura: ",  all['temperature'][0]['value'])
            print("Umidade: ",  all['temperature'][1]['value'])

            print("Total de pessoas na sala: ", all['totalPeople'])
            print()
            allPeopleInBuilding = allPeopleInBuilding + all['totalPeople']
            alarmSystem = all['alarm']
            fireAlarmSystem = all['fireAlarm']
        print("Total de pessoas no prédio: ", allPeopleInBuilding)
        print("Sistema de alarme: ", alarmSystem)
        print("Sistema de alarme de incêndio: ", fireAlarmSystem)
            
        