import socket
import json
import os
from time import sleep
from distributed import Distributed

class Connection:
    def __init__(self, host: str, port: str, portDistributed: str, numberOfConnections: int) -> None:
        self.host = host
        self.port = port
        self.portDistributed = portDistributed
        self.numberOfConnections = numberOfConnections
        self.distributedList: list[Distributed] = []

    def receiveConnection(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            sock.bind((self.host, self.port))
        except socket.error as msg:
            print("Erro ao criar socket: ", msg)
            exit(1)

        sock.listen(self.numberOfConnections)

        while True:
            connection, client_address = sock.accept()

            try:
                while True:
                    data = connection.recv(2048)
                        
                    if data:
                        decodedData = json.loads(data.decode('UTF-8'))

                    else:
                        break
                    
            finally:
                self.setInformationFromConnection(client_address[0], decodedData)
                connection.close()
    
    def setInformationFromConnection(self, adress: str, information):
        for distributed in self.distributedList:
            if(adress == distributed.ip):
                distributed.information = information
                return

        self.distributedList.append(Distributed(adress, self.portDistributed, information))

    def convertStringToJsonByte(self, message: dict[str, any]):
        byteData = json.dumps(message)
        byteData = bytes(byteData, encoding="UTF-8")
        return byteData

    def sendInformation(self, target: Distributed, message: dict[str, any]):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            sock.connect((target.ip, target.port))
        except:
            return

        try:
            sock.sendall(self.convertStringToJsonByte(message))
        finally:
            sock.close()