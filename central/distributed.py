class Distributed:
    def __init__(self, ip: str, port: str, information: dict[str, any]) -> None:
        self.ip = ip
        self.port = port
        self.information: dict[str, any] = information
        pass