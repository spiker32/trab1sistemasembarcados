import socket
import json
import os
from connection import Connection
import threading
from time import sleep
from terminal import Terminal

# Portas 10371 -> 10380

jsonfile = open('configuracao_sala_01.json')
jsondata = json.load(jsonfile)

numberOfConnections: int = 4

host: str = jsondata['ip_servidor_central']
port: str = jsondata['porta_servidor_central']
portDistributedServer = jsondata['porta_servidor_distribuido']

def main():
    connections = Connection(host, port, portDistributedServer, 4)
    terminal = Terminal(connections)

    try:
        t1 = threading.Thread(target = connections.receiveConnection, args = ())
        t1.start()

        t2 = threading.Thread(target = terminal.menu, args = ())
        t2.start()
        
    except:
        print ("Error: unable to start threads")
        return

    t1.join()
    t2.join()

main()